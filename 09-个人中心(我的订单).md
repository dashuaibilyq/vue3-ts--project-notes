## 个人中心

### 个人中心-路由配置

![image-20210822062307681](asset/image-20210822062307681.png)

（1）创建组件`src/views/Member/Layout/index.vue`

```vue
<template>
  <div>个人中心布局</div>
</template>

<script>
export default {}
</script>

<style></style>

```

（2）配置路由

```js
children: [
    ...,
    { path: '/member', component: () => import('@/views/Member/Layout') }
]
```

（3）修改首页头部导航的链接

```jsx
<li>
  <RouterLink to="/member" ><i class="iconfont icon-user"></i>
  	{{ profile.nickname || profile.account }}
  </RouterLink>
</li>

<li><RouterLink to="/member">会员中心</RouterLink></li>
```

### 个人中心-布局容器

> 目的：完成个人中心页面基础架子，配置路由。

![image-20210822063714663](asset/image-20210822063714663.png)

大致步骤：

- 准备个人中心左菜单组件
- 准备个人中心布局容器组件
- 准备个人中心首页组件
- 配置路由规则

落地代码：

1. 准备个人中心左菜单组件

`src/views/Member/Layout/components/member-aside.vue`

```vue
<template>
  <div class="xtx-member-aside">
    <div class="user-manage">
      <h4>我的账户</h4>
      <div class="links">
        <a href="javascript:;">个人中心</a>
        <a href="javascript:;">消息通知</a>
        <a href="javascript:;">个人信息</a>
        <a href="javascript:;">安全设置</a>
        <a href="javascript:;">地址管理</a>
        <a href="javascript:;">我的积分</a>
        <a href="javascript:;">我的足迹</a>
        <a href="javascript:;">邀请有礼</a>
        <a href="javascript:;">幸运抽奖</a>
      </div>
      <h4>交易管理</h4>
      <div class="links">
        <a href="javascript:;">我的订单</a>
        <a href="javascript:;">优惠券</a>
        <a href="javascript:;">礼品卡</a>
        <a href="javascript:;">评价晒单</a>
        <a href="javascript:;">售后服务</a>
      </div>
      <h4>我的收藏</h4>
      <div class="links">
        <a href="javascript:;">收藏的商品</a>
        <a href="javascript:;">收藏的专题</a>
        <a href="javascript:;">关注的品牌</a>
      </div>
      <h4>帮助中心</h4>
      <div class="links">
        <a href="javascript:;">帮助中心</a>
        <a href="javascript:;">在线客服</a>
      </div>
    </div>
  </div>
</template>

<script>
export default {
  name: 'XtxMemberAside'
}
</script>

<style scoped lang='less'>
.xtx-member-aside {
  width: 220px;
  margin-right: 20px;
  border-radius: 2px;
  .user-manage {
    background-color: #fff;
    h4 {
      font-size: 18px;
      font-weight: 400;
      padding: 20px 52px 5px;
      border-top: 1px solid #f6f6f6;
    }

    .links {
      padding: 0 52px 10px;
    }

    a {
      display: block;
      line-height: 1;
      padding: 15px 0;
      font-size: 14px;
      color: #666;
      position: relative;

      &:hover{
        color: @xtxColor;
      }
      &.active {
        color: @xtxColor;

        &:before {
          display: block;
        }
      }

      &:before {
        content: "";
        display: none;
        width: 6px;
        height: 6px;
        border-radius: 50%;
        position: absolute;
        top: 19px;
        left: -16px;
        background-color: @xtxColor;
      }
    }
  }
}
</style>
```

2. 准备个人中心布局容器组件

`src/views/Member/Layout/index.vue`

```vue
<template>
  <div class="container">
    <MemberAside />
    <div class="article">
      <!-- 三级路由的出口 -->
      <RouterView />
    </div>
  </div>
</template>
<script>
import MemberAside from './components/member-aside.vue'
export default {
  name: 'Member',
  components: {
    MemberAside
  }
}
</script>

<style scoped lang="less">
.container {
  display: flex;
  padding-top: 20px;
  .article {
    width: 1000px;
  }
}
</style>

```

3. 准备个人中心首页组件 和 订单组件

`src/views/Member/Home/index.vue`  

```vue
<template>
  <div class="member-home">
      个人中心
  </div>
</template>
<script>
export default {
  name: 'MemberHome'
}
</script>
<style scoped lang="less">

</style>
```

`src/views/Member/Order/index.vue`

```jsx
<template>
  <div class="member-order">
      订单列表
  </div>
</template>
<script>
export default {
  name: 'MemberOrder'
}
</script>
<style scoped lang="less">

</style>
```

4. 配置路由规则

```jsx
  {
    path: '/',
    component: Layout,
    children: [
      ...
      {
        path: '/member',
        component: () => import('@/views/Member/Layout'),
        children: [
          { path: '', component: () => import('@/views/Member/Home') },
          { path: 'order', component: () => import('@/views/Member/Order') }
        ]
      }
    ]
  },
```



### 个人中心-菜单激活

> 目标：动态激活左侧菜单

大致步骤：

- 添加个人中心路由地址，设置精准匹配类名
- 添加我的订单路由与组件

```text
router-link-active         当你的路由路径包含 router-link组件的to属性值，当前组件会加上它
router-link-exact-active   当你的路由路径完全和你的router-link组件的to属性值一致，当前组件会加上它
```

`src/components/app-member-aside.vue`

```jsx
<RouterLink to="/member">个人中心</RouterLink>

<RouterLink to="/member/order">我的订单</RouterLink>
```

类名修改

```js
&.router-link-exact-active {
  color: @xtxColor;

  &:before {
    display: block;
  }
}
```



### 个人中心-基础布局

> 完成个人中心-首页基础布局

大致步骤：

- 准备概览组件
- 组件面板组件

落地代码：

1. 准备概览组件

`src/views/Member/Home/components/home-overview.vue`

```vue
<template>
  <!-- 概览 -->
  <div class="home-overview">
    <!-- 用户信息 -->
    <div class="user-meta">
      <div class="avatar">
        <img src="http://zhoushugang.gitee.io/erabbit-client-pc-static/uploads/avatar_1.png"/>
      </div>
      <h4>徐菲菲</h4>
    </div>
    <div class="item">
      <a href="javascript:;">
        <span class="iconfont icon-hy"></span>
        <p>会员中心</p>
      </a>
      <a href="javascript:;">
        <span class="iconfont icon-aq"></span>
        <p>安全设置</p>
      </a>
      <a href="javascript:;">
        <span class="iconfont icon-dw"></span>
        <p>地址管理</p>
      </a>
    </div>
  </div>
</template>
<script>
export default {
  name: 'MemberHomeOverview'
}
</script>
<style scoped lang="less">
.home-overview {
  height: 132px;
  background: url(~@/assets/images/center-bg.png) no-repeat center / cover;
  display: flex;
  .user-meta {
    flex: 1;
    display: flex;
    align-items: center;
    .avatar {
      width: 85px;
      height: 85px;
      border-radius: 50%;
      overflow: hidden;
      margin-left: 60px;
      img {
        width: 100%;
        height: 100%;
      }
    }
    h4 {
      padding-left: 26px;
      font-size: 18px;
      font-weight: normal;
      color: white;
    }
  }
  .item {
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: space-around;
    &:first-child {
      border-right: 1px solid #f4f4f4;
    }
    a {
      color: white;
      font-size: 16px;
      text-align: center;
      .iconfont {
        font-size: 32px;
      }
      p {
        line-height: 32px;
      }
    }
  }
}
</style>
```



2. 组件面板组件

`src/views/Member/Home/components/home-panel.vue`

```vue
<template>
  <div class="home-panel">
    <div class="header">
      <h4>{{ title }}</h4>
      <XtxMore to="/" />
    </div>
    <!-- 商品列表 -->
    <div class="goods-list"></div>
  </div>
</template>
<script>
export default {
  name: 'MemberHomeOrder',
  props: {
    title: {
      type: String,
      default: ''
    }
  }
}
</script>
<style scoped lang="less">
.home-panel {
  background-color: #fff;
  padding: 0 20px;
  margin-top: 20px;
  height: 400px;
  .header {
    height: 66px;
    border-bottom: 1px solid #f5f5f5;
    padding: 18px 0;
    display: flex;
    justify-content: space-between;
    align-items: baseline;
    h4 {
      font-size: 22px;
      font-weight: normal;
    }
  }
}
</style>
```



3. 使用面板组件，猜你喜欢组件

`src/views/member/home/index.vue`

```vue
<template>
  <div class="member-home">
    <HomeOverview />
    <HomePanel title="收藏的商品"  />
    <HomePanel title="我的足迹" />
  </div>
</template>
<script>
import HomeOverview from './components/home-overview'
import HomePanel from './components/home-panel'
export default {
  name: 'MemberHome',
  components: {
    HomeOverview,
    HomePanel
  }
}
</script>
<style scoped lang="less">
:deep(.xtx-carousel) .carousel-btn.prev {
  left: 5px;
}
:deep(.xtx-carousel) .carousel-btn.next {
  right: 5px;
}
</style>
```



### 个人中心-渲染页面

> 目的：用户信息展示，面板内商品展示。

`src/views/member/home/components/home-overview.vue`

```vue
    <!-- 用户信息 -->
    <div class="user-meta">
      <div class="avatar">
        <img :src="$store.state.user.profile.avatar"/>
      </div>
      <h4>{{$store.state.user.profile.account}}</h4>
    </div>
```

`src/views/member/home/components/home-panel.vue`

```vue
<template>
  <div class="home-panel">
    <div class="header">
      <h4>{{ title }}</h4>
      <XtxMore to="/" />
    </div>
    <!-- 商品列表 -->
    <div class="goods-list">
      <GoodsItem v-for="i in 4" :key="i" :goods="goods" />
    </div>
  </div>
</template>
<script>
import GoodsItem from '@/views/Category/components/goods-item'
export default {
  name: 'MemberHomeOrder',
  components: { GoodsItem },
  props: {
    title: {
      type: String,
      default: ''
    }
  },
  setup () {
    const goods = {
      id: '1147023',
      name: '自煮火锅不排队 麦饭石不粘鸳鸯火锅',
      picture: 'https://yanxuan-item.nosdn.127.net/fcdcb840a0f5dd754bb8fd2157579012.jpg',
      desc: '清汤鲜香 红汤劲爽',
      price: '159.00'
    }
    return { goods }
  }
}
</script>
<style scoped lang="less">
.home-panel {
  background-color: #fff;
  padding: 0 20px;
  margin-top: 20px;
  height: 400px;
  .header {
    height: 66px;
    border-bottom: 1px solid #f5f5f5;
    padding: 18px 0;
    display: flex;
    justify-content: space-between;
    align-items: baseline;
    h4 {
      font-size: 22px;
      font-weight: normal;
    }
  }
}

.goods-list {
  display: flex;
  justify-content: space-around;
  padding-top: 20px;
}
</style>
```







## 订单管理

![image-20210822072536117](asset/image-20210822072536117.png)

### 订单管理 - tabs组件

> 目的：封装一个高可用tabs组件。类似于：https://element-plus.gitee.io/zh-CN/component/tabs.html

大致步骤：

- `xtx-tabs` 组件容器 可以有多个 `xtx-tabs-panel` 组件面板
- `xtx-tabs-panel` 支持：  标题 `props.label`，名称 `props.name`，内容 `<slot />`
- `xtx-tabs`  组件组织结构，控制点击事件，标签页激活。

铺垫知识：

新建 Hello组件 => 演示 jsx语法

- `jsx` 语法，需要简单了解其基本使用 https://v3.cn.vuejs.org/guide/render-function.html#jsx

#### 没有 jsx 的 render 函数

代码演示1：

```html
<script>
import { createVNode } from 'vue'
export default {
  name: 'Hello',
  render () {
    // return createVNode(标签名, { 属性名: 属性值 }, 子节点)
    return createVNode('div', { id: 'box' }, 'hello')
  }
}
</script>
```

代码演示2：

```html
<script>
import { createVNode } from 'vue'
export default {
  name: 'Hello',
  render () {
    // return createVNode(标签名, { 属性名: 属性值 }, 子节点)
    return createVNode('div', { id: 'box' }, [
      createVNode('p', {}, '我是一个段落1'),
      createVNode('p', {}, '我是一个段落2')
    ])
  }
}
</script>
```

思考：渲染以下结构？

```jsx
<ul>
  <li>111</li>
  <li>222</li>
  <li>333</li>
  <li>444</li>
</ul>
```



#### 有了 jsx 的 render 函数

```jsx
<script>
export default {
  name: 'Hello',
  render () {
    return <div>
      <p>我是一个段落1</p>
      <p>我是一个段落2</p>
    </div>
  }
}
</script>
```

语法说明：jsx中可以嵌套 js 的语法，但是需要用 {} 标记出来

例如：

```jsx
<script>
export default {
  name: 'Hello',
  render () {
    const fn = () => {
      return '你好'
    }
    const name = 'zs'
    const clickFn = function () {
      console.log('你好')
    }
    const isLoading = true
    const arr = [11, 22, 33, 44]
    const div = <span>abc</span>

    if (!isLoading) {
      return <div>
        <p>{fn()}</p>
        <p>{name}</p>
        <button onClick={clickFn}>按钮</button>
      </div>
    } else {
      return <div>
        <p>loading</p>
        <p>{arr}</p>
        {div}
      </div>
    }
  }
}
</script>
```

#### 封装 tabs 组件 - 准备组件

准备两个组件

`src/components/Tabs/panel.vue`

```vue
<template>
  <div class="xtx-tabs-panel">
    <slot></slot>
  </div>
</template>

<script>
export default {
  name: 'XtxTabsPanel',
  props: {
    label: {
      type: String,
      default: ''
    },
    name: {
      type: String,
      default: ''
    }
  },
  setup () {
    return {

    }
  }
}
</script>
```

`src/components/Tabs/index.vue`

```jsx
<script>
export default {
  name: 'XtxTabs',
  render () {
    return <div class="xtx-tabs">tabs</div>
  }
}
</script>
```



#### 封装 tabs 组件 - 准备组件

通过 `this.$slots.default()` 获取插槽， 渲染内容

```jsx
<script>
export default {
  name: 'XtxTabs',
  render () {
    const panels = this.$slots.default()
    const navs = (
      <nav>
        <a href="javascript:;">标题1</a>
        <a href="javascript:;">标题2</a>
        <a href="javascript:;">标题3</a>
        <a href="javascript:;">标题4</a>
        <a href="javascript:;">标题5</a>
      </nav>
    )
    return <div class="xtx-tabs">{[navs, panels]}</div>
  }
}
</script>
```

nav 头部导航，应该基于 `this.$slots.default()` 动态渲染

```jsx
<script>
export default {
  name: 'XtxTabs',
  render () {
    const panels = this.$slots.default()
    const navs = (
      <nav>
        {
          panels.map(item => {
            return (
              <a href="javascript:;">{item.props.label}</a>
            )
          })
        }
      </nav>
    )
    return <div class="xtx-tabs">{[navs, panels]}</div>
  }
}
</script>
```

#### 封装 tabs 组件 - 支持 v-model

```jsx
<XtxTabs v-model="activeName">
   ...
</XtxTabs>
```

```jsx
  props: {
    modelValue: {
      type: String,
      default: ''
    }
  },
  setup (props, { emit }) {
    const clickFn = (item, index) => {
      emit('update:modelValue', item.props.name)
    }
    return {
      clickFn
    }
  },
  render () {
   const navs = (
      <nav>
        {
          panels.map((item, index) => {
            return (
              <a
                class={{ active: this.modelValue === item.props.name }}
                onClick={() => this.clickFn(item, index)}
                href="javascript:;">
                {item.props.label}
              </a>
            )
          })
        }
      </nav>
    )
  	...
  }
```

#### 封装 tabs 组件 -  面板切换

tabs组件

```jsx
  setup (props, { emit }) {
    ...
    
    // 解构时会丢失响应式特性
    const { modelValue } = toRefs(props)
    provide('activeName', modelValue)

    ...
  },
```

tabs-panel组件接收

```jsx
<template>
  <div class="xtx-tabs-panel" v-show="activeName === name">
    <slot></slot>
  </div>
</template>

<script>
import { inject } from 'vue'
export default {
  name: 'XtxTabsPanel',
  props: {
    label: {
      type: String,
      default: ''
    },
    name: {
      type: String,
      default: ''
    }
  },
  setup () {
    const activeName = inject('activeName')
    return {
      activeName
    }
  }
}
</script>
```



#### 封装 tabs 组件 - v-for 渲染

```js
// 订单状态
const orderStatus = [
  { name: 'all', label: '全部订单' },
  { name: 'unpay', label: '待付款' },
  { name: 'deliver', label: '待发货' },
  { name: 'receive', label: '待收货' },
  { name: 'comment', label: '待评价' },
  { name: 'complete', label: '已完成' },
  { name: 'cancel', label: '已取消' }
]
```

组件升级：

- 动态判断节点类型，组织tabs列表

```jsx
const arr = this.$slots.default()
const panels = []

arr.forEach(item => {
  // 处理基本的写法，直接内部写 XtxTabsPanel
  if (item.type.name === 'XtxTabsPanel') {
    panels.push(item)
  } else {
    // 处理v-for的情况
    if (item.children && item.children[0]?.type?.name === 'XtxTabsPanel') {
      // 一个节点 => 7个panel
      item.children.forEach(panel => {
        panels.push(panel)
      })
    }
  }
})
```



#### 支持 tab-click  - 完整代码

`src/components/Tabs/index.vue`

```vue
<script>
import { provide, toRefs } from 'vue'
export default {
  name: 'XtxTabs',
  props: {
    modelValue: {
      type: String,
      default: ''
    }
  },
  setup (props, { emit }) {
    const clickFn = (item, index) => {
      // console.log('点击了导航')
      // 子传父，更新到父组件的 modelValue
      emit('update:modelValue', item.props.name)
      emit('tab-click', {
        tab: item,
        index: index
      })
    }

    // 响应式数据的能力：在props身上
    // 传递过去的，不是一个响应式的数据，变了，是不会更新的
    const { modelValue } = toRefs(props)
    provide('activeName', modelValue)

    return {
      clickFn
    }
  },

  render () {
    const arr = this.$slots.default()
    const panels = []

    arr.forEach(item => {
      // 处理基本的写法，直接内部写 XtxTabsPanel
      if (item.type.name === 'XtxTabsPanel') {
        panels.push(item)
      } else {
        // 处理v-for的情况
        if (item.children && item.children[0]?.type?.name === 'XtxTabsPanel') {
          // 一个节点 => 7个panel
          item.children.forEach(panel => {
            panels.push(panel)
          })
        }
      }
    })

    const nav = (
      <nav>
        {
          panels.map((item, index) => {
            return (
              <a
                class={ { active: this.modelValue === item.props.name } }
                href="javascript:;"
                onClick={() => this.clickFn(item, index)}
              >
                {item.props.label}
              </a>
            )
          })
        }
      </nav>
    )

    return (
      <div class="xtx-tabs">
        {[nav, panels]}
      </div>
    )
  }
}
</script>

<style lang="less">
.xtx-tabs {
  background: #fff;
  > nav {
    height: 60px;
    line-height: 60px;
    display: flex;
    border-bottom: 1px solid #f5f5f5;
    > a {
      width: 110px;
      border-right: 1px solid #f5f5f5;
      text-align: center;
      font-size: 16px;
      &.active {
        border-top: 2px solid @xtxColor;
        height: 60px;
        background: #fff;
        line-height: 56px;
      }
    }
  }
}
</style>

```

`src/components/Tabs/panel.vue`

```vue
<template>
  <div class="xtx-tabs-panel" v-show="activeName === name">
    <slot></slot>
  </div>
</template>

<script>
import { inject } from 'vue'
export default {
  name: 'XtxTabsPanel',
  props: {
    label: {
      type: String,
      default: ''
    },
    name: {
      type: String,
      default: ''
    }
  },
  setup () {
    const activeName = inject('activeName')
    return { activeName }
  }
}
</script>
```



### 订单管理 - 使用 Tabs 组件

`src/constants/order.js`   订单状态常量数据

```js
// 订单状态
export const orderStatus = [
  { name: 'all', label: '全部订单' },
  { name: 'unpay', label: '待付款' },
  { name: 'deliver', label: '待发货' },
  { name: 'receive', label: '待收货' },
  { name: 'comment', label: '待评价' },
  { name: 'complete', label: '已完成' },
  { name: 'cancel', label: '已取消' }
]
```

`src/views/Member/Order/index.vue`

```vue
<template>
  <div class="member-order-page">
    <XtxTabs v-model="activeName">
      <XtxTabsPanel
        v-for="item in orderStatus"
        :key="item.name"
        :label="item.label"
        :name="item.name"
        >
          {{item.label}}
       </XtxTabsPanel>
    </XtxTabs>
  </div>
</template>
<script>
import { ref } from 'vue'
import { orderStatus } from '@/constants/order'
export default {
  name: 'MemberOrderPage',
  setup () {
    const activeName = ref('all')
    return { activeName, orderStatus }
  }
}
</script>

<style scoped lang="less"></style>
```



### 订单管理 - 基础布局

> 目标：完成订单静态布局。
>
> 不需要把订单列表和分页都放到单独的tabs中。

![image-20210822072943219](asset/image-20210822072943219.png)

基础样式：

```less
.order-list {
  background: #fff;
  padding: 20px;
}
.order-item {
  margin-bottom: 20px;
  border: 1px solid #f5f5f5;
  .head {
    height: 50px;
    line-height: 50px;
    background: #f5f5f5;
    padding: 0 20px;
    overflow: hidden;
    span {
      margin-right: 20px;
      &.down-time {
        margin-right: 0;
        float: right;
        i {
          vertical-align: middle;
          margin-right: 3px;
        }
        b {
          vertical-align: middle;
          font-weight: normal;
        }
      }
    }
    .del {
      margin-right: 0;
      float: right;
      color: #999;
    }
  }
  .body {
    display: flex;
    align-items: stretch;
    .column {
      border-left: 1px solid #f5f5f5;
      text-align: center;
      padding: 20px;
      > p {
        padding-top: 10px;
      }
      &:first-child {
        border-left: none;
      }
      &.goods {
        flex: 1;
        padding: 0;
        align-self: center;
        ul {
          li {
            border-bottom: 1px solid #f5f5f5;
            padding: 10px;
            display: flex;
            &:last-child {
              border-bottom: none;
            }
            .image {
              width: 70px;
              height: 70px;
              border: 1px solid #f5f5f5;
            }
            .info {
              width: 220px;
              text-align: left;
              padding: 0 10px;
              p {
                margin-bottom: 5px;
                &.name {
                  height: 38px;
                }
                &.attr {
                  color: #999;
                  font-size: 12px;
                  span {
                    margin-right: 5px;
                  }
                }
              }
            }
            .price {
              width: 100px;
            }
            .count {
              width: 80px;
            }
          }
        }
      }
      &.state {
        width: 120px;
        .green {
          color: @xtxColor;
        }
      }
      &.amount {
        width: 200px;
        .red {
          color: @priceColor;
        }
      }
      &.action {
        width: 140px;
        a {
          display: block;
          &:hover {
            color: @xtxColor;
          }
        }
      }
    }
  }
}
```

基础结构：

```vue
    <div class="order-list">
      <div class="order-item">
        <div class="head">
          <span>下单时间：2018-01-08 15:02:00</span>
          <span>订单编号：62205697599</span>
          <span class="down-time">
            <i class="iconfont icon-down-time"></i>
            <b>付款截止：28分20秒</b>
          </span>
        </div>
        <div class="body">
          <div class="column goods">
            <ul>
              <li v-for="i in 2" :key="i">
                <a class="image" href="javascript:;">
                  <img src="https://yanxuan-item.nosdn.127.net/f7a4f643e245d03771d6f12c94e71214.png" alt="" />
                </a>
                <div class="info">
                  <p class="name ellipsis-2">原创设计一体化机身,精致迷你破壁机350mL</p>
                  <p class="attr ellipsis">
                    <span>颜色：绿色</span>
                    <span>尺寸：10寸</span>
                  </p>
                </div>
                <div class="price">¥9.50</div>
                <div class="count">x1</div>
              </li>
            </ul>
          </div>
          <div class="column state">
            <p>待付款</p>
          </div>
          <div class="column amount">
            <p class="red">¥19.00</p>
            <p>（含运费：¥10.00）</p>
            <p>在线支付</p>
          </div>
          <div class="column action">
            <XtxButton type="primary" size="small">立即付款</XtxButton>
            <p><a href="javascript:;">查看详情</a></p>
            <p><a href="javascript:;">取消订单</a></p>
          </div>
        </div>
      </div>
            <div class="order-item">
        <div class="head">
          <span>下单时间：2018-01-08 15:02:00</span>
          <span>订单编号：62205697599</span>
          <a href="javascript:;" class="del">删除</a>
        </div>
        <div class="body">
          <div class="column goods">
            <ul>
              <li>
                <a class="image" href="javascript:;">
                  <img src="https://yanxuan-item.nosdn.127.net/f7a4f643e245d03771d6f12c94e71214.png" alt="" />
                </a>
                <div class="info">
                  <p class="name ellipsis-2">原创设计一体化机身,精致迷你破壁机350mL</p>
                  <p class="attr ellipsis">
                    <span>颜色：绿色</span>
                    <span>尺寸：10寸</span>
                  </p>
                </div>
                <div class="price">¥9.50</div>
                <div class="count">x1</div>
              </li>
            </ul>
          </div>
          <div class="column state">
            <p>已取消</p>
          </div>
          <div class="column amount">
            <p class="red">¥9.50</p>
            <p>（含运费：¥0.00）</p>
          </div>
          <div class="column action">
            <p><a href="javascript:;">查看详情</a></p>
          </div>
        </div>
      </div>
    </div>
```

### 订单管理 - 列表渲染

> 目标：完成订单列表默认渲染。

大致步骤：

- 定义 API 接口函数
- 单条订单封装组件
- 获取数据进行渲染

落地代码：

1. 获取订单列表，API接口

```js
/**
 * 查询订单列表
 * @param {Number} orderState - 订单状态，1为待付款、2为待发货、3为待收货、4为待评价、5为已完成、6为已取消，未传该参数或0为全部
 * @param {Number} page - 页码
 * @param {Number} pageSize - 每页条数
 * @returns
 */
export const reqFindOrderList = ({ orderState, page, pageSize }) => {
  return request('/member/order', 'get', { orderState, page, pageSize })
}
```

2. 组件初始化获取订单信息

```html
<script>
import { reactive, ref } from 'vue'
import { reqFindOrderList } from '@/api/order'
import { orderStatus } from '@/constants/order'
export default {
  name: 'MemberOrderPage',
  setup () {
    const activeName = ref('all')
    // 查询订单参数
    const requestParams = reactive({
      orderState: 0,
      page: 1,
      pageSize: 5
    })
    // 订单列表
    const orderList = ref([])
    // 查询订单
    async function getOrderList () {
      const res = await reqFindOrderList(requestParams)
      orderList.value = res.result.items
    }
    getOrderList()
    return { activeName, orderStatus, orderList }
  }
}
</script>
```

3. 提取组件 order-item `src/views/Order/components/order-item.vue`

```scss
<template>
  <div class="order-item">
    <div class="head">
      <span>下单时间：2018-01-08 15:02:00</span>
      <span>订单编号：62205697599</span>
      <span class="down-time">
        <i class="iconfont icon-down-time"></i>
        <b>付款截止：28分20秒</b>
      </span>
    </div>
    <div class="body">
      <div class="column goods">
        <ul>
          <li v-for="i in 2" :key="i">
            <a class="image" href="javascript:;">
              <img
                src="https://yanxuan-item.nosdn.127.net/f7a4f643e245d03771d6f12c94e71214.png"
                alt=""
              />
            </a>
            <div class="info">
              <p class="name ellipsis-2">
                原创设计一体化机身,精致迷你破壁机350mL
              </p>
              <p class="attr ellipsis">
                <span>颜色：绿色</span>
                <span>尺寸：10寸</span>
              </p>
            </div>
            <div class="price">¥9.50</div>
            <div class="count">x1</div>
          </li>
        </ul>
      </div>
      <div class="column state">
        <p>待付款</p>
      </div>
      <div class="column amount">
        <p class="red">¥19.00</p>
        <p>（含运费：¥10.00）</p>
        <p>在线支付</p>
      </div>
      <div class="column action">
        <XtxButton type="primary" size="small">立即付款</XtxButton>
        <p><a href="javascript:;">查看详情</a></p>
        <p><a href="javascript:;">取消订单</a></p>
      </div>
    </div>
  </div>
</template>

<script>
export default {}
</script>

<style lang="less" scoped>

.order-item {
  margin-bottom: 20px;
  border: 1px solid #f5f5f5;
  .head {
    height: 50px;
    line-height: 50px;
    background: #f5f5f5;
    padding: 0 20px;
    overflow: hidden;
    span {
      margin-right: 20px;
      &.down-time {
        margin-right: 0;
        float: right;
        i {
          vertical-align: middle;
          margin-right: 3px;
        }
        b {
          vertical-align: middle;
          font-weight: normal;
        }
      }
    }
    .del {
      margin-right: 0;
      float: right;
      color: #999;
    }
  }
  .body {
    display: flex;
    align-items: stretch;
    .column {
      border-left: 1px solid #f5f5f5;
      text-align: center;
      padding: 20px;
      > p {
        padding-top: 10px;
      }
      &:first-child {
        border-left: none;
      }
      &.goods {
        flex: 1;
        padding: 0;
        align-self: center;
        ul {
          li {
            border-bottom: 1px solid #f5f5f5;
            padding: 10px;
            display: flex;
            &:last-child {
              border-bottom: none;
            }
            .image {
              width: 70px;
              height: 70px;
              border: 1px solid #f5f5f5;
            }
            .info {
              width: 220px;
              text-align: left;
              padding: 0 10px;
              p {
                margin-bottom: 5px;
                &.name {
                  height: 38px;
                }
                &.attr {
                  color: #999;
                  font-size: 12px;
                  span {
                    margin-right: 5px;
                  }
                }
              }
            }
            .price {
              width: 100px;
            }
            .count {
              width: 80px;
            }
          }
        }
      }
      &.state {
        width: 120px;
        .green {
          color: @xtxColor;
        }
      }
      &.amount {
        width: 200px;
        .red {
          color: @priceColor;
        }
      }
      &.action {
        width: 140px;
        a {
          display: block;
          &:hover {
            color: @xtxColor;
          }
        }
      }
    }
  }
}
</style>

```

4. 使用order-item组件 `order/index.vue`

```jsx
import OrderItem from './components/order-item'
components: { OrderItem },
```

```vue
<div class="order-list">
    <OrderItem v-for="item in orderList" :key="item.id" :order="item" />
</div>
```

5. 动态渲染 order-item

![](docs/media/20210830142653183.png)

```html
<template>
  <div class="order-item" v-if="order.id">
    <div class="head">
      <span>下单时间：{{ order.createTime }}</span>
      <span>订单编号：{{ order.id }}</span>
      <!-- 未付款，倒计时时间还有 -->
      <span class="down-time" v-if="order.orderState===1">
        <i class="iconfont icon-down-time"></i>
        <b>付款截止：{{ countTimeText }}</b>
      </span>
    </div>
    <div class="body">
      <div class="column goods">
        <ul>
          <li v-for="item in order.skus" :key="item.id">
            <a class="image" href="javascript:;">
              <img
                :src="item.image"
                alt=""
              />
            </a>
            <div class="info">
              <p class="name ellipsis-2">
                {{ item.name }}
              </p>
              <p class="attr ellipsis">
                <span>{{ item.attrsText }}</span>
              </p>
            </div>
            <div class="price">¥{{ item.realPay }}</div>
            <div class="count">x{{ item.quantity }}</div>
          </li>
        </ul>
      </div>
      <div class="column state">
        <p>{{ orderStatus[order.orderState].label }}</p>
        <p v-if="order.orderState===3"><a href="javascript:;" class="green">查看物流</a></p>
        <p v-if="order.orderState===4"><a href="javascript:;" class="green">评价商品</a></p>
        <p v-if="order.orderState===5"><a href="javascript:;" class="green">查看评价</a></p>
      </div>
      <div class="column amount">
        <p class="red">¥{{ order.payMoney }}</p>
        <p>（含运费：¥{{ order.postFee }}）</p>
        <p>在线支付</p>
      </div>
      <div class="column action">
        <XtxButton v-if="order.orderState===1" type="primary" size="small">立即付款</XtxButton>
        <XtxButton v-if="order.orderState===3" type="primary" size="small">确认收货</XtxButton>
        <p><a href="javascript:;">查看详情</a></p>
        <p v-if="[2,3,4,5].includes(order.orderState)"><a href="javascript:;">再次购买</a></p>
        <p v-if="[4,5].includes(order.orderState)"><a href="javascript:;">申请售后</a></p>
        <p v-if="order.orderState===1"><a href="javascript:;">取消订单</a></p>
      </div>
    </div>
  </div>
</template>

<script>
import { orderStatus } from '@/constants/order'
import { useCountDown } from '@/compositions'
export default {
  props: {
    order: {
      type: Object,
      default: () => ({})
    }
  },
  setup (props) {
    const { countTime, countTimeText, start } = useCountDown()
    start(props.order.countdown)
    return {
      orderStatus,
      countTime,
      countTimeText
    }
  }
}
</script>

<style lang="less" scoped>

.order-item {
  margin-bottom: 20px;
  border: 1px solid #f5f5f5;
  .head {
    height: 50px;
    line-height: 50px;
    background: #f5f5f5;
    padding: 0 20px;
    overflow: hidden;
    span {
      margin-right: 20px;
      &.down-time {
        margin-right: 0;
        float: right;
        i {
          vertical-align: middle;
          margin-right: 3px;
        }
        b {
          vertical-align: middle;
          font-weight: normal;
        }
      }
    }
    .del {
      margin-right: 0;
      float: right;
      color: #999;
    }
  }
  .body {
    display: flex;
    align-items: stretch;
    .column {
      border-left: 1px solid #f5f5f5;
      text-align: center;
      padding: 20px;
      > p {
        padding-top: 10px;
      }
      &:first-child {
        border-left: none;
      }
      &.goods {
        flex: 1;
        padding: 0;
        align-self: center;
        ul {
          li {
            border-bottom: 1px solid #f5f5f5;
            padding: 10px;
            display: flex;
            &:last-child {
              border-bottom: none;
            }
            .image {
              width: 70px;
              height: 70px;
              border: 1px solid #f5f5f5;
            }
            .info {
              width: 220px;
              text-align: left;
              padding: 0 10px;
              p {
                margin-bottom: 5px;
                &.name {
                  height: 38px;
                }
                &.attr {
                  color: #999;
                  font-size: 12px;
                  span {
                    margin-right: 5px;
                  }
                }
              }
            }
            .price {
              width: 100px;
            }
            .count {
              width: 80px;
            }
          }
        }
      }
      &.state {
        width: 120px;
        .green {
          color: @xtxColor;
        }
      }
      &.amount {
        width: 200px;
        .red {
          color: @priceColor;
        }
      }
      &.action {
        width: 140px;
        a {
          display: block;
          &:hover {
            color: @xtxColor;
          }
        }
      }
    }
  }
}
</style>

```



### 订单管理 -  切换列表

模拟发货：http://pcapi-xiaotuxian-front-devtest.itheima.net/member/order/consignment/:id

```jsx
<XtxTabs v-model="activeName" @tab-click="tabClick">
  <XtxTabsPanel
    v-for="item in orderStatus"
    :key="item.name"
    :label="item.label"
    :name="item.name">
  </XtxTabsPanel>
</XtxTabs>

const tabClick = ({ tab, index }) => {
  requestParams.orderState = index
}

watch(requestParams, () => {
  getOrderList()
})
```







## 知识点拓展补充

1. `script setup` 组合 API 语法糖

   - 官方文档：https://v3.cn.vuejs.org/api/sfc-script-setup.html

2. `provide` 和 `inject` 依赖注入

   1. 官方文档：https://v3.cn.vuejs.org/guide/component-provide-inject.html
   2. 提供数据：`provide('key', value)`
   3. 获取数据：`const value = inject('key')`

   > 应用价值：
   >
   > 1. 祖先级要传递数据给后代组件。（`props` 只能传递给子组件，`Vuex` 用起来又太麻烦）
   > 2. 无法`props` 实现传值的场景。（如插槽结构传值）

3. `createVNode` 和 `render`

   1. 官方文档：
   2. `Vue3` 创建虚拟DOM通过  `createVNode`或  `h` 函数
   3. `Vue2` 创建虚拟DOM通过  `createElement` 或 `h`  函数

4. `JSX` 语法糖（React团队设计发明的，Vue 也进行了支持）

   1. 其实就是`createVNode` 的语法糖，负责创建虚拟DOM用的。底层依赖 `babel` 插件支持。

   > 应用价值：
   >
   > 1. 允许在 JS 环境中通过写标签的方式创建虚拟 DOM
   > 2. 虚拟DOM要配合 render 渲染函数才会变成真实的 DOM

5. `SKU` 组件核心步骤解读

   1. 测试商品1：http://www.corho.com:8080/#/product/1369155859933827074
   2. 测试商品2：http://www.corho.com:8080/#/product/1379052170040578049

   > 业务核心：
   >
   >  	1. 根据 `SKUID` 集合制作查询字典 (依赖幂集算法)
   >  	2. 当用户点击某个规格的时候，通过程序模拟用户下一次选择，如果在字典中查询不到，更新为禁用状态。



